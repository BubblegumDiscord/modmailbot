const Discord = require('discord.js');
const client = new Discord.Client();
const fs = require("fs");

client.on('ready', () => {
  console.log(`Logged in as ${client.user.tag}!`);
});

client.on('message', async (msg) => {
  if (msg.content.startsWith("!dm")) {
    if (msg.content.split(" ").length < 3) return;
    var id = msg.content.split(" ")[1];
    var m = msg.content.split(" ").slice(2).join(" ");
    var usr = await client.fetchUser(msg.content.split(" ")[1])
    var dm = await usr.createDM();
    dm.send(m)
  }
});

client.login(JSON.parse(fs.readFileSync("./config.json", "utf-8")).token);
